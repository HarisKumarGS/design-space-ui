export const environment = {
  production: true,
  endpointUrl : 'https://v8kqjmjqej.execute-api.us-east-1.amazonaws.com/prod',
  githubEndpoint : 'https://api.github.com/repos',
  githubUsername : 'HarisKumarGS',
  githubComponentContentsUrl : 'design-space/contents/design-space/src/app/components',
  mainBranch: 'master'
};
