import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ComponentService {

  htmlFileExtension = 'html';
  scssFileExtension = 'scss';
  tsFileExtension = 'ts';

  constructor(private http: HttpClient) { }

  getAllComponentsOverview() {
    return this.http.get(`${environment.endpointUrl}/get-all-components-overview`);
  }

  getAllDesignSystems() {
    return this.http.get(`${environment.endpointUrl}/get-all-design-systems-overview`);
  }

  getHtmlCode(component: any) {
    return this.http.get(`${environment.githubEndpoint}/${environment.githubUsername}/${environment.githubComponentContentsUrl}/${component}/${component}.component.${this.htmlFileExtension}?ref=${environment.mainBranch}`);
  }

  getScssFile(component: any) {
    return this.http.get(`${environment.githubEndpoint}/${environment.githubUsername}/${environment.githubComponentContentsUrl}/${component}/${component}.component.${this.scssFileExtension}?ref=${environment.mainBranch}`);
  }
  
  getTsFile(component: any) {
    return this.http.get(`${environment.githubEndpoint}/${environment.githubUsername}/${environment.githubComponentContentsUrl}/${component}/${component}.component.${this.tsFileExtension}?ref=${environment.mainBranch}`);
  }
}
