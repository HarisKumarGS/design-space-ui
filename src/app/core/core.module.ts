import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentService } from './services/component.service';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [ComponentService]
})
export class CoreModule { }
