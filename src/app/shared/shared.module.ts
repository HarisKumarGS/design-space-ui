import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderToolbarComponent } from './header-toolbar/header-toolbar.component';
import { ShortCardComponent } from './short-card/short-card.component';
import { NbCardModule } from '@nebular/theme';
import { InfoCardComponent } from './info-card/info-card.component';
import { HttpClientModule } from '@angular/common/http';
import { ComponentCardComponent } from './component-card/component-card.component';


@NgModule({
  declarations: [
    HeaderToolbarComponent, 
    ShortCardComponent, 
    InfoCardComponent, 
    ComponentCardComponent
  ],
  imports: [
    CommonModule,
    NbCardModule,
    HttpClientModule
  ],
  exports: [
    HeaderToolbarComponent,
    ShortCardComponent,
    InfoCardComponent,
    ComponentCardComponent
  ]
})
export class SharedModule { }
