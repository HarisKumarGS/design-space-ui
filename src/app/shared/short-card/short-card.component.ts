import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-short-card',
  templateUrl: './short-card.component.html',
  styleUrls: ['./short-card.component.scss']
})
export class ShortCardComponent implements OnInit {

  @Input() theme: string = 'light';
  @Input() title: string = '';
  @Input() link: string = '';
  @Output() cardClick = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  onCardClick() {
    this.cardClick.emit(this.link);
  }

}
