import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-info-card',
  templateUrl: './info-card.component.html',
  styleUrls: ['./info-card.component.scss']
})
export class InfoCardComponent implements OnInit {
  @Input() url: string = '';

  // linkPreviewCallback = (route: string) => {
  //   return this.http.get(route).pipe(
  //     map((x: any)=>console.log(x)))
  // }

  public apiCallbackFn = (route: any) => {
    // return this.http.get(route);

    // Hard code for demo purposes:
    return this.http.get('./assets/open-graph-test/medium-angular.json');
 };

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

}
