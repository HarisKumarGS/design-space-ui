import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-component-card',
  templateUrl: './component-card.component.html',
  styleUrls: ['./component-card.component.scss']
})
export class ComponentCardComponent implements OnInit {

  @Input() title: string = '';
  @Input() description: string = '';
  @Input() icon: string = '';
  @Output() cardClick = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  onCardClick() {
    this.cardClick.emit(this.title);
  }
}
