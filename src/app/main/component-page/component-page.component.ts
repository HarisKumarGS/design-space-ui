import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ComponentService } from 'src/app/core/services/component.service';
import { AvatarComponent } from 'design-space-material';

@Component({
  selector: 'app-component-page',
  templateUrl: './component-page.component.html',
  styleUrls: ['./component-page.component.scss']
})
export class ComponentPageComponent implements OnInit {

  renderComponent = AvatarComponent;

  component: string | null= '';
  componentDetails: any;
  loading = false;

  htmlCode: string= "<!-- HTML Content goes here -->";
  stylingCode: string = '<!-- Stylings goes here -->';
  componentCode: string = '<!-- Logics goes here -->';


  constructor(private router: Router, private activatedRoute: ActivatedRoute, private componentService: ComponentService) { 
    this.component = this.activatedRoute.snapshot.paramMap.get('component');
    this.activatedRoute.params.subscribe((params)=> {
      this.componentDetails = params['componentData'];
    })
  }

  ngOnInit(): void {
    this.getComponentDetails();
    this.getComponentCode();
  }

  getComponentDetails() {
    this.loading = true;
    this.componentService.getAllComponentsOverview().subscribe((components: any)=> {
      let componentDetails = components.filter((element: any) => element.title === this.component);
      this.componentDetails = componentDetails;
      this.loading = false;
    });
  }

  getComponentCode() {
    this.loading = true;
    const component = this.component?.toLowerCase();
    this.componentService.getHtmlCode(component).subscribe((htmlCode: any) => {
      this.htmlCode = `${atob(htmlCode.content)}`;
      this.loading = false;
    });
    this.componentService.getScssFile(component).subscribe((stylingCode: any) => {
      if(stylingCode.content.length !== 0) {
        this.stylingCode = `${atob(stylingCode.content)}`;
      } else {
        this.stylingCode = '';
      }
      this.loading = false;
    });
    this.componentService.getTsFile(component).subscribe((componentCode: any) => {
      this.componentCode = `${atob(componentCode.content)}`;
      this.loading = false;
    });
  }

  navigateToHome() {
    this.router.navigate(['home']);
  }
}
