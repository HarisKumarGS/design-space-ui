import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ComponentService } from 'src/app/core/services/component.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  designSystems:any[] = [];
  components:any[] = [];
  loading = false;

  knowledgeBase:any[] = [];
  icons:any[] = [];

  constructor(private router: Router, private componentService: ComponentService) { }

  ngOnInit(): void {
    this.getOverviews();
  }

  getOverviews() {
    this.loading = true;
    this.componentService.getAllDesignSystems().subscribe((designSystems: any)=> {
      this.designSystems = designSystems;
      this.loading = false;
    })
    this.componentService.getAllComponentsOverview().subscribe((components: any)=> {
      this.components = components;
      this.loading = false;
    })
  }

  navigateTo(link: string) {
    window.location.href = link;
  }

  navigateToComponentPage(title: string) {
    const componentData = this.components.filter(element => element.title === title);
    this.router.navigate([`components/${title}`, componentData]);
  }
}
