import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { SharedModule } from '../shared/shared.module';
import { MatGridListModule } from '@angular/material/grid-list';
import { ComponentPageComponent } from './component-page/component-page.component';
import { NbCardModule, NbSpinnerModule, NbTabsetModule } from '@nebular/theme';
import {MatTabsModule} from '@angular/material/tabs';
import { FormsModule } from '@angular/forms';
import { HighlightModule, HIGHLIGHT_OPTIONS } from 'ngx-highlightjs';
import { AvatarModule } from 'design-space-material';
import { DynamicModule, DynamicIoModule } from 'ng-dynamic-component';

@NgModule({
  declarations: [HomeComponent, ComponentPageComponent],
  imports: [
    CommonModule,
    AvatarModule,
    DynamicModule,
    DynamicIoModule,
    MatGridListModule,
    NbCardModule,
    NbSpinnerModule,
    NbTabsetModule,
    FormsModule,
    HighlightModule,
    MatTabsModule,
    SharedModule
  ],
  providers: [
    {
      provide: HIGHLIGHT_OPTIONS,
      useValue: {
        fullLibraryLoader: () => import('highlight.js'),
      }
    }
  ],
  exports: [HomeComponent, ComponentPageComponent]
})
export class MainModule { }
